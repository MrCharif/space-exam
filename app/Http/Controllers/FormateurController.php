<?php

namespace App\Http\Controllers;

use App\Group;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Reponse;
use App\Models\UserGroup;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\GroupUse;
use Validator;

class FormateurController extends Controller {


	public function __construct() {
		$this->middleware( 'auth' );
	}

	public function index() {
		$user = Auth::user();
		$role = $user->getUserRole;

		//return response()->json($user);
		return view( 'pages/home' )->with( [ 'user' => $user, 'role' => $role ] );
	}

	public function getExams() {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}


		$exams = $user->getExamsOrders( 'desc' )->get();

		$data = [
			'user'  => $user,
			'role'  => $role,
			'exams' => $exams
		];

		//return response()->json($data);

		return view( 'pagesFormateur.examsList' )->with( $data );
	}

	public function getExam( Exam $exam ) {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}

		$data = [
			'user'  => $user,
			'role'  => $role,
			'exam'  => $exam,
			'saved' => false,
			'error' => ''
		];

		return view( 'pagesFormateur.updateExam' )->with( $data );
	}

	public function addNewExamIndex( Request $request ) {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}

		$isSaved = false;
		$error   = "";

		$data = [
			'user'  => $user,
			'role'  => $role,
			'saved' => $isSaved,
			'error' => $error,
			'exam'  => null
		];

		if ( $request->isMethod( 'post' ) ) {

			$validator = Validator::make( $request->all(), [
				'exam_name'   => 'bail|required',
				'exam_type'   => 'bail|required',
				'time_min'    => 'bail|required',
				'date_depart' => 'bail|required',
				'date_fin'    => 'required'
			] );


			if ( ! $validator->fails() ) {
				$exam               = new Exam();
				$exam->exam_name    = $request->input( 'exam_name' );
				$exam->exam_type    = $request->input( 'exam_type' );
				$exam->time_min     = $request->input( 'time_min' );
				$exam->date_depart  = Carbon::parse( $request->input( 'date_depart' ) )->toDateTimeString();
				$exam->date_fin     = Carbon::parse( $request->input( 'date_fin' ) )->toDateTimeString();
				$exam->formateur_id = $user->id;

				if ( $exam->save() ) {
					$data['saved'] = true;
					$data['exam']  = $exam;
				} else {
					$data['error'] = "Error lorsque de l'enregistrement";
				}
			} else {
				return redirect( route( 'formateurAddNewExams' ) )
					->withErrors( $validator )
					->withInput();
			}

		}


		return view( 'pagesFormateur.addNewExam' )->with( $data );
	}

	public function updateExam( Request $request, Exam $exam ) {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}

		$isSaved = false;
		$error   = "";

		$data = [
			'user'  => $user,
			'role'  => $role,
			'saved' => $isSaved,
			'error' => $error,
			'exam'  => $exam
		];

		if ( $exam->formateur_id == $user->id ) {

			$validator = Validator::make( $request->all(), [
				'exam_name'   => 'bail|required',
				'exam_type'   => 'bail|required',
				'time_min'    => 'bail|required',
				'date_depart' => 'bail|required',
				'date_fin'    => 'required'
			] );

			if ( ! $validator->fails() ) {
				$exam->exam_name   = $request->input( 'exam_name' );
				$exam->exam_type   = $request->input( 'exam_type' );
				$exam->time_min    = $request->input( 'time_min' );
				$exam->date_depart = Carbon::parse( $request->input( 'date_depart' ) )->toDateTimeString();
				$exam->date_fin    = Carbon::parse( $request->input( 'date_fin' ) )->toDateTimeString();

				if ( $exam->save() ) {
					$data['saved'] = true;
					$data['exam']  = $exam;
				} else {
					$data['error'] = "Error lorsque de l'update";
				}
			} else {
				return redirect( route( 'formateurExamInfo', [ 'exam' => $exam->id ] ) )
					->withErrors( $validator )
					->withInput()
					->with( $data );
			}

		}


		return redirect( route( 'formateurExamInfo', [ 'exam' => $exam->id ] ) )
			->with( $data );
	}

	public function deleteExam( Exam $exam ) {
		$user = Auth::user();
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}

		if ( $exam->formateur_id = $user->id ) {
			$exam->delete();

			return redirect( route( 'formateurExams' ) );
		}

		return redirect( route( 'formateurExams' ) )
			->withErrors( [ 'message' => 'permission diened' ] );
	}

	public function examQuestions( Exam $exam ) {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isFormateur() ) {
			return redirect()->route( 'home' );
		}

		$questions = $exam->getQuestions;



		$questions->each( function ( $question ) {
			$reponse = $question->getReponses;

			//$question->addRep = route('formateurAddReponse', ['$question' => $question->id]);
		} );

		$data = [
			'user' => $user,
			'role' => $role,
			'exam' => $exam->toArray()
		];

		//return response()->json($data);

		return view( 'pagesFormateur.examQuestion' )->with( $data );

	}

	public function getExamQuestion( $id ) {

	}

	public function addQuestion( Request $request, Exam $exam ) {
		$validator = Validator::make( $request->all(), [
			'question' => 'bail|required',
			'point'    => 'bail|required',
			'type'     => 'bail|required'
		] );

		$data = [
			'error'   => false,
			'message' => null,
			'data'    => null
		];

		if ( ! $validator->fails() ) {
			$question           = new Question();
			$question->question = $request->input( 'question' );
			$question->point    = $request->input( 'point' );
			$question->type     = $request->input( 'type' );
			$question->id_exam  = $exam->id;

			$question->save();

			$data['error']   = false;
			$data['message'] = 'question_added';
			$data['data']    = $question;

		} else {
			$data['error']   = true;
			$data['message'] = 'error_validation';
			$data['data']    = $validator->errors();
		}

		return response()->json( $data );

	}

	public function deleteQuestion( Request $request, Question $question ) {

		$question->delete();

		$data['error']   = false;
		$data['message'] = 'question_deleted';
		$data['data']    = $question;

		return response()->json( $data );

	}


	public function updateReponse( $id_Reponse, Request $request ) {
		$msg          = "";
		$reponse      = Reponse::find( $id_Reponse );
		$formateur_id = Question::find( $reponse->id_exam )->getExam->formateur_id;
		if ( $formateur_id == Auth::id() ) {
			$reponse->reponse = $request->input( "reponse" );
			$reponse->is_true = $request->input( "is_true" );
			$reponse->type    = $request->input( "id_question" );
			$reponse->update();
			$msg = "Updated with Success";
		} else {
			$msg = "Can't Update";
		}

		return response()->json( $msg );
	}

	public function addReponse( Request $request, Question $question ) {
		$validator = Validator::make( $request->all(), [
			'reponse' => 'bail|required',
			'is_true' => 'bail|required'
		] );

		$data = [
			'error'   => false,
			'message' => null,
			'data'    => null
		];


		if ( ! $validator->fails() ) {
			$reponse              = new Reponse();
			$reponse->reponse     = $request->input( 'reponse' );
			$reponse->is_true     = $request->input( 'is_true' ) ? 1 : 0;
			$reponse->id_question = $question->id;
			$reponse->save();

			$data['error']   = false;
			$data['message'] = 'question_added';
			$data['data']    = $question;
		} else {
			$data['error']   = true;
			$data['message'] = 'error_validation';
			$data['data']    = $validator->errors();
		}

		return response()->json( $data );
	}

	public function AddUserToGroup( $user_id, $group_id ) {
		$msg     = '';
		$user    = Auth::user();
		$usergrp = new UserGroup;
		if ( $user->isFormateur() ) {
			$usergrp->user_id  = $user_id;
			$usergrp->group_id = $group_id;
			$usergrp->save();
			$msg = 'Added';
		} else {
			$msg = 'Not Autorized';
		}

		return response()->json( $msg );
	}

	public function ExamsResult() {
		$user = Auth::id();
		$user->getExams;

	}
}
