<?php

namespace App\Http\Controllers\user;

use App\Models\Exam;
use App\Models\ExamenArchive;
use App\Models\Examiner;
use App\Models\Question;
use App\Models\Reponse;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ExamsController extends Controller {
	public function __construct() {
		$this->middleware( 'auth' );
	}

	public function indexExamObligate() {
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}

		$exams = Exam::with(['getFormateur'])->get();

		$data = [
			'user'  => $user,
			'role'  => $role,
			'exams' => $exams
		];

		//return response()->json($data);

		return view( 'pages/exams' )->with($data);

	}

	public function indexExamTest() {
		$user = Auth::user();
		$role = $user->getUserRole;

		$user = Auth::user();
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}

		$exams = Exam::all();

		//TODO: Cours from DB create Table first
		return view( 'pages/exams' )->with(
			[
				'user'  => $user,
				'role'  => $role,
				'exams' => $exams
			]
		);

	}

	public function indexExamInfo( Exam $exam ) {
		$user = Auth::user();
		$role = $user->getUserRole;

		$user = Auth::user();
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}

		session()->forget( 'qurentQuestion' );

		$question = $exam->questionRand;

		$index = 0;
		foreach ( $question as $qu ) {
			$qu->position = $index;
			$ques         = Question::find( $qu->id );
			$ques->reponsesRand;

			$qu->reponses = $ques->reponsesRand ? $ques->reponsesRand : [];
			$index ++;
		}


		$StartTime = Carbon::now();
		$EndTime   = Carbon::now()->addMinute( $exam->time_min );
		$examiner  = Examiner::create( [
			'id_user'    => $user->id,
			'id_exam'    => $exam->id,
			'status'     => 'started',
			'date_start' => $StartTime->toDateTimeString(),
			'date_fin'   => $EndTime->toDateTimeString()
		] );

		$exID = $examiner->id;
		session( [ 'examiner_' . $user->id . '_' . $exam->id . '_' . $exID => $exam->toJson() ] );

		//return response()->json($exam);

		//TODO: Cours from DB create Table first
		return view( 'pages/examInformation' )->with(
			[
				'user'       => $user,
				'role'       => $role,
				'exam'       => $exam,
				'examinerID' => encrypt( $exID ),
			]
		);
	}


	public function indexExamStart( Request $request, Exam $exam, $exID ) {
		$user = Auth::user();
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}
		$role = $user->getUserRole;

		$qurentQuestion = session( 'qurentQuestion', 0 );

		$exID = decrypt( $exID );

		$input = $request->all();
		if ( $input != null && $request->isMethod( 'post' ) ) {

			$reponsesIDS = $request->input( 'reponses' );
			$questionID  = $request->input( 'question' );
			$calcul_Question = Question::find( $questionID );

			if($calcul_Question->type == 'box'){
				foreach ( $reponsesIDS as $reponse ) {

					$this->saveArchive($calcul_Question, $reponse, $exID, $questionID);
				}
			}else{
				$this->saveArchive($calcul_Question, $reponsesIDS, $exID, $questionID);
			}

			$qurentQuestion ++;
		}

		$examiner_0001 = session( 'examiner_' . $user->id . '_' . $exam->id . '_' . $exID, null );


		$data = [
			'qurentQuestion' => $qurentQuestion,
			'user'           => $user,
			'examinerID'     => encrypt( $exID ),
			'role'           => $role,
			'exam'           => $exam,
			'question'       => null,
			'archive'        => null,
			'score'          => 0,
			'totalPoint'     => 0,
			'percentage'     => 0
		];

		if ( !($examiner_0001 == null) ) {
			$exam        = json_decode( $examiner_0001 );
			$exam->typee = "session";

			$question = null;

			session( [ 'qurentQuestion' => $qurentQuestion ] );

			$data['exam'] = $exam;

			try {

				$question = $exam->question_rand[ $qurentQuestion ];
				$data['question'] = $question;

			} catch ( ErrorException $e ) {
				$question = null;
				session()->forget( 'qurentQuestion' );
				session()->forget( 'examiner_' . $user->id . '_' . $exam->id . '_' . $exID );



				$ExamArchive = Examiner::where('id', $exID)->first();

				$arrrrr = $ExamArchive->getExamsArchive;
				$score = 0;
				$totalpoint = 0;
				foreach ($arrrrr as $archh){
					$score = $score + $archh->score;
					$queee = $archh->getQuestion;
					$totalpoint = $totalpoint + $queee->point;
					$archh->getReponse;
				}


				$ExamArchive->status = 'finished';
				$ExamArchive->score = round(($score * 100) / $totalpoint);
				$ExamArchive->save();

				$data['totalPoint'] = $totalpoint;
				$data['score'] = $score;
				$data['percentage'] = round(($score * 100) / $totalpoint) . '%';
				$data['archive'] = $ExamArchive;

				$data['archive'] = $ExamArchive;
			}
		}
		else {
			$question = null;

			$ExamArchive = Examiner::where('id', $exID)->first();

			$arrrrr = $ExamArchive->getExamsArchive;
			$score = 0;
			$totalpoint = 0;
			foreach ($arrrrr as $archh){
				$score = $score + $archh->score;
				$queee = $archh->getQuestion;
				$totalpoint = $totalpoint + $queee->point;
				$archh->getReponse;
			}

			$ExamArchive->status = 'finished';
			$ExamArchive->score = round(($score * 100) / $totalpoint);
			$ExamArchive->save();

			$data['totalPoint'] = $totalpoint;
			$data['score'] = $score;
			$data['percentage'] = round(($score * 100) / $totalpoint) . '%';
			$data['archive'] = $ExamArchive;
		}


		unset( $exam->question_rand );


		//return response()->json($data);

		return view( 'pages/examStart' )->with( $data );
	}

	private function saveArchive($calcul_Question, $reponse, $exID, $questionID){
		$reponses        = $calcul_Question->reponses;

		$nbrTrueRep = 0;
		foreach ($reponses as $repons){
			if($repons->is_true){
				$nbrTrueRep ++;
			}
		}

		$axaAr = ExamenArchive::where('examiner_id', $exID)
		                      ->where('question_id', $questionID)
		                      ->where('reponse_id', $reponse)
		                      ->exists();

		if($axaAr){
			//die('exist deja this rep');
			return false;
		}

		$quRep = Reponse::find( $reponse );

		$exaArchive              = new ExamenArchive();
		$exaArchive->examiner_id = $exID;
		$exaArchive->question_id = $questionID;
		$exaArchive->reponse_id  = $reponse;
		$exaArchive->is_rep_true = $quRep->is_true;
		$exaArchive->nbr_true_rep = $nbrTrueRep;

		if(($quRep->is_true == 1) ? true : false){
			$exaArchive->score = $calcul_Question->point / $nbrTrueRep;
		}
		else{
			$exaArchive->score = 0;
		}

		return $exaArchive->save();
	}


	public function userResult(){
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}


		$Examiner = Examiner::where('id_user', $user->id)
			->with(['getExam'])
			->get();

		$data = [
			'user'  => $user,
			'role'  => $role,
			'results' => $Examiner
		];

		//return response()->json($data);

		return view( 'pages/myResults' )->with( $data );
	}

	public function resultInfo( Exam $exam, $exID){
		$user = Auth::user();
		$role = $user->getUserRole;
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}

		$exID = decrypt( $exID );


		$data = [
			'qurentQuestion' => 0,
			'user'           => $user,
			'examinerID'     => encrypt( $exID ),
			'role'           => $role,
			'exam'           => $exam,
			'question'       => null,
			'archive'        => null,
			'score'          => 0,
			'totalPoint'     => 0,
			'percentage'     => 0
		];

		$ExamArchive = Examiner::where('id', $exID)->first();

		$arrrrr = $ExamArchive->getExamsArchive;
		$score = 0;
		$totalpoint = 0;
		foreach ($arrrrr as $archh){
			$score = $score + $archh->score;
			$queee = $archh->getQuestion;
			$totalpoint = $totalpoint + $queee->point;
			$archh->getReponse;
		}

		$ExamArchive->status = 'finished';
		$ExamArchive->score = round(($score * 100) / $totalpoint);
		$ExamArchive->save();

		$data['totalPoint'] = $totalpoint;
		$data['score'] = $score;
		$data['percentage'] = round(($score * 100) / $totalpoint) . '%';
		$data['archive'] = $ExamArchive;

		return view( 'pages/examStart' )->with( $data );
	}
}
