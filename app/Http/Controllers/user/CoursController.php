<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoursController extends Controller {
	public function __construct() {
		$this->middleware( 'auth' );
	}

	public function index() {
		$user = Auth::user();
		$role = $user->getUserRole;

		$user = Auth::user();
		if ( ! $user->isUser() ) {
			return redirect()->route( 'home' );
		}

		//TODO: Cours from DB create Table first
		return view( 'pages/cours' )->with(
			[
				'user' => $user,
				'role' => $role ,
				'cours' => []
			]
		);

	}
}
