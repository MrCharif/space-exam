<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Question
 *
 * @property int $id
 * @property string $question
 * @property string $point
 * @property string $type
 * @property int $id_exam
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reponse[] $reponses
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereIdExam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question wherePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Question extends Model
{
    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question','point', 'type', 'id_exam'
    ];

    public function getReponses(){
        return $this->hasMany(Reponse::class,'id_question');
    }

    public function getExam(){
        return $this->belongsTo(Exam::class,'id_exam');
    }

	public function reponses(){
		return $this->hasMany('App\Models\Reponse', 'id_question', 'id');
	}

	public function reponsesRand(){
		return $this->reponses()->inRandomOrder();
	}

}
