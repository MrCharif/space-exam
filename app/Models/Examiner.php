<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Examiner
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_exam
 * @property string $status
 * @property string $date_start
 * @property string $date_fin
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereDateFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereIdExam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $score
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Examiner whereScore($value)
 */
class Examiner extends Model
{
    protected $table = 'examiner';
    //
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user','id_exam', 'status', 'date_start', 'date_fin', 'score'
    ];

    public function getUser(){
        return $this->belongsTo(User::class,'id_user');
    }

    public function getExam(){
        return $this->belongsTo(Exam::class,'id_exam');
    }

    public function getExamsArchive(){
        return $this->hasMany(ExamenArchive::class);
    }

	public function getExamsArchiveGrouped(){
		return $this->getExamsArchive()->orderBy('question_id');
	}
}
