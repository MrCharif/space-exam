<?php

namespace App\Models;

use App\Group;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $nom
 * @property string|null $prenom
 * @property string $email
 * @property string $password
 * @property int $role_id
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePrenom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserRole(){
        return $this->hasOne(Role::class, 'id','role_id');
    }

    public function getExams(){
        return $this->hasMany(Exam::class,'formateur_id');
    }

	public function getExamsOrders($type = 'desc'){
		return $this->getExams()->orderBy('id', $type);
	}

    public function getExaminer(){
        return $this->hasMany(Examiner::class,'id_user');
    }

	// left Group  relationship
	public function getGroup()
	{
		return $this->belongsToMany(Group::class)
		            ->withTimestamps();
	}


    public function isAdmin(){
    	$role = $this->getUserRole;
    	return (strtolower($role->name) == strtolower("admin")) ? 1 : 0;
    }

	public function isUser(){
		$role = $this->getUserRole;
		return (strtolower($role->name) == strtolower("user")) ? 1 : 0;
	}

	public function isFormateur(){
		$role = $this->getUserRole;
		return (strtolower($role->name) == strtolower("formateur")) ? 1 : 0;
	}
}
