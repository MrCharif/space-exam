<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExamenArchive
 *
 * @property int $id
 * @property int $examiner_id
 * @property int $question_id
 * @property int $reponse_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereExaminerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereReponseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_rep_true
 * @property string $type
 * @property int $nbr_true_rep
 * @property int|null $score
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereIsRepTrue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereNbrTrueRep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExamenArchive whereType($value)
 */
class ExamenArchive extends Model
{
    //
    protected $table = 'exam_archive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'examiner_id','question_id', 'reponse_id', 'is_rep_true', 'type', 'nbr_true_rep', 'score'
    ];

    //protected $hidden = ['is_rep_true', 'nbr_true_rep'];

    public function getQuestion(){
        return $this->belongsTo(Question::class,'question_id');
    }

    public function getReponse(){
        return $this->belongsTo(Reponse::class,'reponse_id');
    }
    public function getExaminer(){
        return $this->belongsTo(Examiner::class,'examiner_id');
    }
}
