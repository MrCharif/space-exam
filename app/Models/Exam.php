<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Exam
 *
 * @property int $id
 * @property string $exam_name
 * @property string $exam_type
 * @property int $time_min
 * @property string $date_depart
 * @property string $date_fin
 * @property int $formateur_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $questions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereDateDepart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereDateFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereExamName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereExamType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereFormateurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereTimeMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Exam extends Model
{
    //


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exam_name','exam_type', 'time_min', 'date_depart', 'date_fin','formateur_id'
    ];

    public function getFormateur(){
        return $this->belongsTo(User::class,'formateur_id');
    }

    public function getQuestions(){
        return $this->hasMany(Question::class,'id_exam');
    }
	public function questions(){
		return $this->hasMany('App\Models\Question', 'id_exam', 'id');
	}

    public function getExaminers(){
        return $this->hasMany(Examiner::class,'id_exam');
    }
	public function questionRand(){
		return $this->questions()->inRandomOrder();
	}

}
