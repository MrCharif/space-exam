<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reponse
 *
 * @property int $id
 * @property string $reponse
 * @property int $is_true
 * @property int $id_question
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereIdQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereIsTrue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereReponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reponse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reponse extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reponse','is_true', 'id_question'
    ];

    public function getExamsArchive(){
        return $this->hasMany(ExamenArchive::class);
    }

    public function getQuestion(){
        return $this->belongsTo(Question::class,'id_question');
    }
}
