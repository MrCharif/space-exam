<div class="menu">
    <div class="logo center">
        <img src="{{asset('images/logo.png')}}" alt="logo">
    </div>

    <div class="items">
        <div class="item">
            <a href="{{ route('home') }}">
                <span class="item-icon"><i class="fa fa-home" aria-hidden="true"></i></span>
                <span class="item-title">Accueil</span>
            </a>
        </div>


        <div class="item">
            <a href="{{ route('cours') }}">
                <span class="item-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
                <span class="item-title">Les Cours</span>
            </a>
        </div>

        <div class="item">
            <a href="{{ route('examsObligate') }}">
                <span class="item-icon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span>
                <span class="item-title">Exam Obliger</span>
            </a>
        </div>

        <!--
        <div class="item">
            <span class="item-icon"><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
            <span class="item-title">Exam Blanche</span>
        </div>-->

        <div class="item">
            <a href="{{ route('myResults') }}">
                <span class="item-icon"><i class="fa fa-line-chart" aria-hidden="true"></i></span>
                <span class="item-title">Mes Resultats</span>
            </a>
        </div>

        <div class="item">
            <span class="item-icon"><i class="fa fa-user-secret" aria-hidden="true"></i></span>
            <span class="item-title">Mon Compt</span>
        </div>
    </div>
</div>