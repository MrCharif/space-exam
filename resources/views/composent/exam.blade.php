<div class="col-sm-6 col-md-4 col-lg-2 cour">
    <a href="{{route('examsInfo', ['exam' => $exam['id']])}}">
        <div class="card card1-70h card-2" style="width: 20rem;">
            <div class="card-block relative100">
                <h4 class="card-title centerHV">{{ $exam['exam_name'] }}

                </h4>

                <span class="author">
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    {{ $exam['getFormateur']['nom'] . ' ' . $exam['getFormateur']['prenom'] }}</span>
            </div>
        </div>
    </a>

</div>