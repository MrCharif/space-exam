<div class="col-sm-6 col-md-4 col-lg-2 cour">
    <a href="{{route('resultInfo',
    ['exam' => $res['getExam']['id'] , 'sesName' => encrypt($res['id'])])
    }}">
        <div class="card card1-70h card-2" style="width: 20rem;">
            <div class="card-block relative100">
                <h4 class="card-title centerHV">
                    {{ $res['getExam']['exam_name'] }}
                </h4>

                <span class="author">{{ $res['score'] }} %</span>
            </div>
        </div>
    </a>

</div>