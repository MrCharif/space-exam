@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('formateurExams') }}">Mes Exams</a></li>
            <li class="breadcrumb-item active">Update : {{ $exam['exam_name'] }} </li>
        </ol>
    </div>


    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($saved)
            <div class="alert alert-success">
                <p>votre exam a ete bien mise a jour</p>
            </div>
        @endif

        <form method="post" action="{{ route('formateurEditExams' , ['exam' => $exam['id']]) }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exam_name">Nom d'exam</label>
                <input class="form-control" id="exam_name" value="{{ $exam['exam_name'] }}" name="exam_name"
                       placeholder="Nom d'exam">
            </div>
            <div class="form-group">
                <label for="exam_type">Type d'exam</label>
                <select class="form-control" id="exam_type" name="exam_type">
                    <option value="oblig" @if($exam['exam_name'] == 'oblig') selected @endif>Obligatoire</option>
                    <option value="blanche" @if($exam['exam_name'] == 'blanche') selected @endif>blanche</option>
                </select>
            </div>
            <div class="form-group">
                <label for="time_min">Durée d'exam (min)</label>
                <input type="number" class="form-control" value="{{ $exam['time_min'] }}" id="time_min" name="time_min"
                       placeholder="30">
            </div>

            <div class="form-group">
                <label for="date_depart">Date Start</label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="{{ $exam['date_depart'] }}" id="date_depart"
                           name="date_depart"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="date_fin">Date Fin</label>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" value="{{ $exam['date_fin'] }}" id="date_fin"
                           name="date_fin"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>


            <div class="form-group action">
                <button type="submit" class="btn btn-default btn-primary">Modifier L"exam</button>
                <button type="reset" class="btn btn-default btn-danger">Clear</button>
            </div>
        </form>

    </div>

@endsection


@section('scripts')

    <script type="text/javascript">
        $(document).ready(function () {


            $('#datetimepicker1').datetimepicker();


            $('#datetimepicker2').datetimepicker();

        });
    </script>
@endsection