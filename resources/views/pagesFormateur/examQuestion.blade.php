@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Accueil</a></li>
            <li class="breadcrumb-item"><a href="{{ route('formateurExams') }}">Mes Exams</a></li>
            <li class="breadcrumb-item active">{{ $exam['exam_name'] }}</li>
        </ol>
    </div>

    <div class="">
        <div class="container">

            <div class="exam-meta-data">
                <h3 class="name">{{ $exam['exam_name'] }}</h3>

                <h5 class="type">
                    <i class="fa fa-podcast" aria-hidden="true"></i>
                    : {{ ($exam['exam_type'] == 'oblig') ? 'obligatoire' : 'test' }}
                </h5>

                <h5 class="time">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    : {{ $exam['time_min'] }}
                </h5>
            </div>

            <div class="exam-full-info">

                <div class="container">

                    <question-rep question="null" :questionId="-1"></question-rep>


                    @foreach($exam['get_questions'] as $question)
                        <question-rep question='{{ json_encode($question) }}'
                                      :question_id="{{$question['id']}}"></question-rep>
                    @endforeach

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')

    <script>
        var urls = {
            baseURL: '{{ route('baseURL') }}',
            add_new_question: '{{ route('formateurAddQuestion', ['exam' => $exam['id']]) }}'
        }
    </script>

    <script src="{{ mix('js/app.js') }}"></script>

@endsection
