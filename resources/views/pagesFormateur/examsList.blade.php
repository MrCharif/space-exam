@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Mes Exams</li>
        </ol>
    </div>

    <div class="">
        <div class="container">

            <div class="axtion-new">
                <a href="{{ route('formateurAddNewExams') }}">
                    <button class="btn btn-default btn-primary">Ajouter nouvel exam</button>
                </a>

            </div>

            <div class="table-responsive myTable">
                <table class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Nom d'exam</th>
                        <th>Type d'exam</th>
                        <th>Durée</th>
                        <th>Date fin</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($exams as $exam)
                        <tr>
                            <td>{{ $exam['id'] }}</td>
                            <td>{{ $exam['exam_name'] }}</td>
                            <td>{{ $exam['exam_type'] }}</td>
                            <td>{{ $exam['time_min'] }} minute</td>
                            <td title="{{ $exam['date_fin'] }}">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($exam['date_fin']))->diffForHumans() }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('formateurExamInfo', ['exam' => $exam['id']]) }}">update</a>
                                        </li>
                                        <li><a href="{{ route('deleteExams', ['exam' => $exam['id']]) }}">delete</a>
                                        </li>
                                        <li><a href="{{ route('formateurExamQuestions', ['exam' => $exam['id']]) }}">Les
                                                Question</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ mix('js/app.js') }}"></script>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>-->
@endsection