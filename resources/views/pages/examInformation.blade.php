@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Exames</a></li>
            <li class="breadcrumb-item active">{{ $exam['exam_name'] }}</li>
        </ol>
    </div>

    <div class="">
        <div class="container">

            <div class="exam-meta-data">
                <h3 class="name">{{ $exam['exam_name'] }}</h3>

                <h5 class="type">
                    <i class="fa fa-podcast" aria-hidden="true"></i>
                    : {{ ($exam['exam_type'] == 'oblig') ? 'obligatoire' : 'test' }}
                </h5>

                <h5 class="time">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    : {{ $exam['time_min'] }}
                </h5>
            </div>

            <div class="exam-full-info">

                <div class="startExam">
                    <a href="{{route('examsStart', ['exam' => $exam['id'], 'sesName' => $examinerID])}}">
                        <button class="btn btn-default btn-primary myBtnCenter"> Start L'exame </button>
                    </a>
                </div>

                <div class="container users-prev-scores">
                    <div class="header">
                        <span>Top 10 Score : </span>
                    </div>
                    <div class="row user">
                        <div class="col-sm-6 col-md-4 col-lg-2 cour">
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                            @include('composent.userScore')
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
