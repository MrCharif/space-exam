@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Accueil</a></li>
            <li class="breadcrumb-item active">Mes Resultats</li>
        </ol>
    </div>

    <div class="">

        <div class="container listCours">
            <div class="row">
                @foreach($results as $res)
                    @include('composent.result')
                @endforeach

            </div>
        </div>
    </div>

@endsection