@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Accueil</a></li>
            <li class="breadcrumb-item"><a href="{{ route('examsObligate') }}">Exames</a></li>
            <li class="breadcrumb-item active">{{ $exam->exam_name }}</li>
        </ol>
    </div>

    <div class="">
        <div class="container">

            <div class="exam-meta-data">
                <h3 class="name">{{ $exam->exam_name }}</h3>

                <h5 class="type">
                    <i class="fa fa-podcast" aria-hidden="true"></i>
                    : {{ ($exam->exam_type == 'oblig') ? 'obligatoire' : 'test' }}
                </h5>

                <h5 class="time">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    : {{ $exam->time_min }}
                </h5>
            </div>

            <div class="exam-full-info">

                <div class="container users-prev-scores">
                    @if($question != null)
                        <div class="header">
                            <span>{{ $question->question }}</span>
                            <span> ponts : {{ $question->point }}</span>
                        </div>
                        <form action="{{ route('examsStart', ['exam' => $exam->id, 'sesName' => $examinerID]) }}"
                              method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="curentposition" value="{{ $qurentQuestion }}">
                            <input type="hidden" name="question" value="{{ $question->id }}">
                            <div class="passage">
                                <div class="reponses">

                                    @if($question->type == 'box')
                                        @foreach($question->reponses as $rep)
                                            <div class="reponse">
                                                <input type="checkbox" value="{{ $rep->id }}" name="reponses[]"
                                                       id="rep_{{ $rep->id }}"/>
                                                <label for="rep_{{ $rep->id }}">{{ $rep->reponse }}</label>
                                            </div>
                                        @endforeach
                                    @endif

                                    @if($question->type == 'radio')
                                        @foreach($question->reponses as $rep)
                                            <div class="reponse">
                                                <input type="radio" id="rep_{{ $rep->id }}" value="{{ $rep->id }}"
                                                       name="reponses">
                                                <label for="rep_{{ $rep->id }}">{{ $rep->reponse }}</label>
                                            </div>
                                        @endforeach
                                    @endif


                                </div>

                                <div class="navigation">
                                    <div class="right">
                                        <button type="submit" class="btn btn-default btn-primary">Suivant</button>
                                    </div>

                                    <!--<div class="left">
                                        <a>
                                            <button class="btn btn-default btn-primary">Précedent</button>
                                        </a>
                                    </div>-->
                                </div>
                            </div>
                        </form>

                    @else

                        <div class="header">
                            <span>{{ $exam->exam_name }}</span>
                            <span class="header-right"> Score : {{ $percentage }}</span>
                        </div>

                        <div class="passage">

                            <div>
                                <p>vous avez obtenir
                                    <span class="score">{{ $score }}</span>
                                    point en total de
                                    <span class="total">{{ $totalPoint }}</span>
                                </p>
                            </div>

                            <div class="reponses">
								<?php $index = 0; $passedQuestion = []; ?>
                                @foreach($archive['getExamsArchive'] as $arch)

                                    @if(!in_array($arch['getQuestion']['id'], $passedQuestion))
										<?php $index ++; ?>

                                        @if($index > 1) </div> @endif

                            <div class="question">
                                <div class="question-meta">
                                    <span class="question_number">{{ $index  }} : </span>
                                    <span class="question-text"> {{ $arch['getQuestion']['question'] }} </span>
                                    <span class="question-point"> points : {{ $arch['getQuestion']['point'] }} </span>
                                </div>
                                <div class="reponse-selected {{ ($arch['getReponse']['is_true'] == 1) ? 'istrue' : 'isfalse' }}">
                                    <span class="question_number">Reponse {{ $arch['getReponse']['id']  }}</span>
                                </div>

                                @else
                                    <div class="reponse-selected {{ ($arch['getReponse']['is_true'] == 1) ? 'istrue' : 'isfalse' }}">
                                        <span class="question_number"> {{ $arch['getReponse']['reponse'] }}</span>
                                    </div>
                                @endif




								<?php array_push( $passedQuestion, $arch['getQuestion']['id'] ); ?>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>

@endsection
