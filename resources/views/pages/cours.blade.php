@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Cours</li>
        </ol>
    </div>

    <div class="">

        <div class="container listCours">
            <div class="row">
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
                @include('composent.cour')
            </div>
        </div>

    </div>
@endsection