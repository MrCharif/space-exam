@extends('layouts.template')

@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Exames</li>
        </ol>
    </div>

    <div class="">

        <div class="container listCours">
            <div class="row">
                @foreach($exams as $exam)
                    @include('composent.exam')
                @endforeach

            </div>
        </div>
    </div>

@endsection