<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>

<div id="app">

    <div class="myContainer">
        @if($role['name'] == 'user')
            @include('composent.menu.menu')
        @elseif($role['name'] == 'formateur')
            @include('composent.menu.formateurMenu')
        @else

        @endif


        <div class="my-center">

            @include('composent.navebare')

            <div class="container">
                @yield('content')
            </div>

        </div>
    </div>
</div>


<!-- Scripts <script src="{{ mix('js/app.js') }}"></script> -->


<script>
    window.Laravel = <?php echo json_encode([
		'csrfToken' => csrf_token(),
	]); ?>
</script>
@yield('scripts')
</body>
</html>