<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('baseURL');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cours', 'user\CoursController@index')->name('cours');

Route::get('/exams/obligate', 'user\ExamsController@indexExamObligate')->name('examsObligate');
Route::get('/exam/{exam}', 'user\ExamsController@indexExamInfo')->name('examsInfo');
Route::match(['get', 'post'], '/exam/{exam}/start/{sesName}', 'user\ExamsController@indexExamStart')->name('examsStart');
Route::get('/results', 'user\ExamsController@userResult')->name('myResults');
Route::get('/results/{exam}/{sesName}', 'user\ExamsController@resultInfo')->name('resultInfo');


Route::get('/formateur/exams', 'FormateurController@getExams')
	->name('formateurExams');

Route::match(['get', 'post'],'/formateur/exam/new', 'FormateurController@addNewExamIndex')
	->name('formateurAddNewExams');

Route::get('/formateur/exam/{exam}', 'FormateurController@getExam')
	->name('formateurExamInfo');

Route::post('/formateur/exam/{exam}/edit', 'FormateurController@updateExam' )
	->name('formateurEditExams');

Route::get('/formateur/exam/{exam}/delete', 'FormateurController@deleteExam')
	->name('deleteExams');


Route::get('/formateur/exam/{exam}/questions', 'FormateurController@examQuestions')
	->name('formateurExamQuestions');

Route::post('/formateur/exam/{exam}/question/add', 'FormateurController@addQuestion')
     ->name('formateurAddQuestion');

Route::post('/formateur/question/{question}/delete', 'FormateurController@deleteQuestion')
     ->name('formateurDeleteQuestion');

Route::post('/formateur/question/{question}/reponse/add', 'FormateurController@addReponse')
     ->name('formateurAddReponse');
