<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExamsArchiveForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('exam_archive', function(Blueprint $table)
	    {
		    $table->foreign('examiner_id', 'foreign_examiner_id_on_ExaminerTable')
		          ->references('id')
		          ->on('examiner')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

		    $table->foreign('question_id', 'foreign_question_id_on_QuestionsTable')
		          ->references('id')
		          ->on('questions')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

		    $table->foreign('reponse_id', 'foreign_reponse_id_on_ReponseTable')
		          ->references('id')
		          ->on('reponses')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
