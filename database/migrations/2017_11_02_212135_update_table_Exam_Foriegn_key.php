<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableExamForiegnKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('exams', function(Blueprint $table)
	    {
		    $table->foreign('formateur_id', 'foreign_user_id_on_ExamTable')
		          ->references('id')
		          ->on('users')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
