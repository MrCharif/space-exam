<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuestionsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('questions', function(Blueprint $table)
	    {
		    $table->foreign('id_exam', 'foreign_exam_id_on_QuestionTable')
		          ->references('id')
		          ->on('exams')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
