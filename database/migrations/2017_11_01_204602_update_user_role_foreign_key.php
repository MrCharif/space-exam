<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserRoleForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function(Blueprint $table)
	    {
		    $table->foreign('role_id', 'foreign_userRoleId_on_Rolestable')
		          ->references('id')
		          ->on('roles')
		          ->onUpdate('CASCADE')
		          ->onDelete('RESTRICT');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
