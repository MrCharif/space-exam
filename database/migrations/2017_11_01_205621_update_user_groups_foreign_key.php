<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserGroupsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('user_groups', function(Blueprint $table)
	    {
		    $table->foreign('group_id', 'foreign_group_id_on_GroupsTable')
		          ->references('id')
		          ->on('groups')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

		    $table->foreign('user_id', 'foreign_user_id_on_UsersTable')
		          ->references('id')
		          ->on('users')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
