<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReponsesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('reponses', function(Blueprint $table)
	    {
		    $table->foreign('id_question', 'foreign_question_id_on_ReponsesTable')
		          ->references('id')
		          ->on('questions')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
