<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('examiner', function (Blueprint $table) {
		    $table->integer('id', true);
		    $table->integer('id_user');
		    $table->integer('id_exam');
		    $table->string('status', 20);
		    $table->dateTime('date_start');
		    $table->dateTime('date_fin');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
