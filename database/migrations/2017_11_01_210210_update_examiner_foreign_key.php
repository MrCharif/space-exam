<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExaminerForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('examiner', function(Blueprint $table)
	    {
		    $table->foreign('id_user', 'foreign_user_id_on_UsersTable_Ex')
		          ->references('id')
		          ->on('users')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

		    $table->foreign('id_exam', 'foreign_exam_id_on_ExamsTable')
		          ->references('id')
		          ->on('exams')
		          ->onUpdate('CASCADE')
		          ->onDelete('CASCADE');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
