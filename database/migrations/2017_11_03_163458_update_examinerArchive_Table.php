<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExaminerArchiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('exam_archive', function(Blueprint $table){

		    $table->boolean('is_rep_true')->default(0);
		    $table->string('type', 10)->default('box');
		    $table->integer('nbr_true_rep');
		    $table->integer('score')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
