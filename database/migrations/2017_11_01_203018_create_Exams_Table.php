<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('exams', function (Blueprint $table) {
		    $table->integer('id', true);
		    $table->string('exam_name');
		    $table->string('exam_type');
		    $table->integer('time_min');
		    $table->dateTime('date_depart');
		    $table->dateTime('date_fin');
		    $table->integer('formateur_id');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
